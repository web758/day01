CREATE Database QLSV;
CREATE TABLE DMKHOA
(
    MAKH Varchar(6),
    TenKhoa Varchar(30)
);
CREATE TABLE SINHVIEN
(
    MaSV Varchar(6),
    HoSV Varchar(30),
    TenSV Varchar(15),

    GioiTinh char(1),
    NgaySinh DateTime,
    NoiSinh Varchar(50),
    DiaChi Varchar(50),
    MaKH Varchar(6),
    HocBong Int
);
